package fixtures

import (
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var labels = map[string]string{
	"app":     "foo",
	"release": "foo-release",
}

var SharedSecretConfigMap1 *corev1.ConfigMap = &corev1.ConfigMap{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "foo-shared-secrets-config-1",
		Namespace: "default",
	},
	Data: map[string]string{
		"foo-service-account-key": `
apiVersion: v1
kind: ServiceAccount
metadata:
  name: foo-account
`,
		"foo-role-key": `
apiVersion: v1
kind: Role
metadata:
  name: foo-role
`,
		"foo-role-binding-key": `
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: foo-role-binding
roleRef:
  kind: Role
  name: foo-role
  apiGroup: rbac.authorization.k8s.io
`,
		"foo-config-map-key": `
apiVersion: batch/v1
kind: Job
spec:
  template:
    spec:
      containers:
        - name: foo-shared-secret-container-1
          image: scratch
      restartPolicy: Never
`,
	},
}

var MigrationConfigMap1 *corev1.ConfigMap = &corev1.ConfigMap{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "foo-migration-config-1",
		Namespace: "default",
	},
	Data: map[string]string{
		"foo-config-map-key": `
apiVersion: batch/v1
kind: Job
spec:
  template:
    spec:
      containers:
        - name: foo-migration-container-1
          image: scratch
      restartPolicy: Never
`,
	},
}

var Job1 *batchv1.Job = &batchv1.Job{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "foo-job-1",
		Namespace: "default",
		Labels: map[string]string{
			"app":     "foo",
			"release": "foo-helm-release",
		},
	},
	Spec: batchv1.JobSpec{
		Template: corev1.PodTemplateSpec{
			Spec: corev1.PodSpec{
				Containers: []corev1.Container{corev1.Container{
					Name:  "foo-job-container-1",
					Image: "scratch",
				}},
				RestartPolicy: corev1.RestartPolicyNever,
			},
		},
	},
}

var Job2 *batchv1.Job = &batchv1.Job{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "bar-job-1",
		Namespace: "default",
	},
	Spec: batchv1.JobSpec{
		Template: corev1.PodTemplateSpec{
			Spec: corev1.PodSpec{
				Containers: []corev1.Container{corev1.Container{
					Name:  "bar-job-container-1",
					Image: "scratch",
				}},
				RestartPolicy: corev1.RestartPolicyNever,
			},
		},
	},
}

var Pod1 *corev1.Pod = &corev1.Pod{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "foo-pod-1",
		Namespace: "default",
	},
	Spec: corev1.PodSpec{
		Containers: []corev1.Container{
			corev1.Container{
				Name:  "foo-pod-1",
				Image: "scratch",
			},
		},
		RestartPolicy: corev1.RestartPolicyNever,
	},
}

var Pod2 *corev1.Pod = &corev1.Pod{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "foo-pod-2",
		Namespace: "default",
	},
	Spec: corev1.PodSpec{
		Containers: []corev1.Container{
			corev1.Container{
				Name:  "foo-pod-2",
				Image: "scratch",
			},
		},
		RestartPolicy: corev1.RestartPolicyNever,
	},
}

var Pod3 *corev1.Pod = &corev1.Pod{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "foo-pod-3",
		Namespace: "default",
	},
	Spec: corev1.PodSpec{
		Containers: []corev1.Container{
			corev1.Container{
				Name:  "foo-pod-3",
				Image: "scratch",
			},
		},
		RestartPolicy: corev1.RestartPolicyNever,
	},
}

var Deployment1 *appsv1.Deployment = &appsv1.Deployment{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "foo-dep-1",
		Namespace: "default",
		Labels: map[string]string{
			"app":     "foo",
			"release": "foo-release",
		},
	},
	Spec: appsv1.DeploymentSpec{
		Selector: &metav1.LabelSelector{
			MatchLabels: labels,
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Labels: labels,
			},
			Spec: corev1.PodSpec{
				Containers: []corev1.Container{
					{
						Name:  "foo-container-1",
						Image: "foo-image-1",
					},
				},
			},
		},
	},
}

var replicas int32 = 4

// StatefulSet1 is a fixture for testing StatefulSets
var StatefulSet1 *appsv1.StatefulSet = &appsv1.StatefulSet{
	ObjectMeta: metav1.ObjectMeta{
		Name:      "foo-statefulset-1",
		Namespace: "default",
		Labels: map[string]string{
			"app":     "foo",
			"release": "foo-helm-release",
		},
	},
	Spec: appsv1.StatefulSetSpec{
		Selector: &metav1.LabelSelector{
			MatchLabels: labels,
		},
		Template: corev1.PodTemplateSpec{
			ObjectMeta: metav1.ObjectMeta{
				Labels: labels,
			},
		},
		Replicas: &replicas,
	},
}
