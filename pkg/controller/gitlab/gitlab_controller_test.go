/*
Copyright 2018 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package gitlab

import (
	"fmt"
	"testing"
	"time"

	"github.com/onsi/gomega"
	gitlabv1beta1 "gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/apis/gitlab/v1beta1"
	"gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/controller/gitlab/fixtures"
	"gitlab.com/gitlab-org/charts/components/gitlab-operator/pkg/version"
	"golang.org/x/net/context"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

var c client.Client

var expectedRequest = reconcile.Request{NamespacedName: types.NamespacedName{Name: "foo", Namespace: "default"}}

const timeout = time.Second * 5
const GitLabClassEnv string = "GITLAB_CLASS"

func TestReconcile(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	crdLabels := map[string]string{
		"controller.gitlab.com": version.GetVersion(),
	}
	crdAnnotatins := map[string]string{
		"gitlab.com/class": "foo",
	}
	instance := &gitlabv1beta1.GitLab{ObjectMeta: metav1.ObjectMeta{Name: "foo", Namespace: "default", Labels: crdLabels, Annotations: crdAnnotatins}}
	// Setup the Manager and Controller.  Wrap the Controller Reconcile function so it writes each request to a
	// channel when it is finished.
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())
	c = mgr.GetClient()

	recFn, requests := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())

	stopMgr, mgrStopped := StartTestManager(mgr, g)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	// Create the GitLab object and expect the Reconcile to be created
	err = c.Create(context.TODO(), instance)
	// The instance object may not be a valid object because it might be missing some required fields.
	// Please modify the instance object by adding required fields and then remove the following if statement.
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}
	g.Expect(err).NotTo(gomega.HaveOccurred())
	defer c.Delete(context.TODO(), instance)
	g.Eventually(requests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))
}

// Reconcile should not run for objects not labelled with the correct version
func TestPredicate(t *testing.T) {
	g := gomega.NewGomegaWithT(t)
	crdLabels := map[string]string{
		"controller.gitlab.com": "x.x",
	}
	crdAnnotatins := map[string]string{
		"gitlab.com/class": "foo",
	}
	instance := &gitlabv1beta1.GitLab{ObjectMeta: metav1.ObjectMeta{Name: "foo", Namespace: "default", Labels: crdLabels, Annotations: crdAnnotatins}}

	// Setup the Manager and Controller.  Wrap the Controller Reconcile function so it writes each request to a
	// channel when it is finished.
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())
	c = mgr.GetClient()

	recFn, requests := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())

	stopMgr, mgrStopped := StartTestManager(mgr, g)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	// Create the GitLab object, (without the expected predicate label), and ensure it doesn't get reconciled
	err = c.Create(context.TODO(), instance)
	// The instance object may not be a valid object because it might be missing some required fields.
	// Please modify the instance object by adding required fields and then remove the following if statement.
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}
	g.Expect(err).NotTo(gomega.HaveOccurred())
	defer c.Delete(context.TODO(), instance)
	g.Consistently(requests, timeout).ShouldNot(gomega.Receive())
}

// Reconcile should not run for objects not annotated with the correct class
func TestAnnotation(t *testing.T) {
	g := gomega.NewGomegaWithT(t)
	crdLabels := map[string]string{
		"controller.gitlab.com": version.GetVersion(),
	}
	crdAnnotatins := map[string]string{
		"gitlab.com/class": "bar",
	}
	instance := &gitlabv1beta1.GitLab{ObjectMeta: metav1.ObjectMeta{Name: "foo", Namespace: "default", Labels: crdLabels, Annotations: crdAnnotatins}}

	// Setup the Manager and Controller.  Wrap the Controller Reconcile function so it writes each request to a
	// channel when it is finished.
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())
	c = mgr.GetClient()

	recFn, requests := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())

	stopMgr, mgrStopped := StartTestManager(mgr, g)

	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	// Create the GitLab object, (without the expected annotation), and ensure it doesn't get reconciled
	err = c.Create(context.TODO(), instance)
	// The instance object may not be a valid object because it might be missing some required fields.
	// Please modify the instance object by adding required fields and then remove the following if statement.
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}
	g.Expect(err).NotTo(gomega.HaveOccurred())
	defer c.Delete(context.TODO(), instance)
	g.Consistently(requests, timeout).ShouldNot(gomega.Receive())
}

func TestSubgroupIsolation(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	instance := &fixtures.GitLab{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "foo",
			Namespace: "default",
		},
	}

	// Setup the Manager.
	mgr, err := manager.New(cfg, manager.Options{})
	g.Expect(err).NotTo(gomega.HaveOccurred())
	c = mgr.GetClient()

	// Setup the controllers (both the actual and the dummy) and wrap
	// their `Reconcile` function so they write each request to a
	// channel when it is finished.
	recFn, requests := SetupTestReconcile(newReconciler(mgr))
	g.Expect(add(mgr, recFn)).NotTo(gomega.HaveOccurred())

	// Dummy controller
	subgrpRecFn, subgrpRequests := SetupTestReconcile(fixtures.NewPrefixedTestReconciler(mgr))
	g.Expect(fixtures.AddPrefixedTestReconciler(mgr, subgrpRecFn)).NotTo(gomega.HaveOccurred())

	stopMgr, mgrStopped := StartTestManager(mgr, g)
	defer func() {
		close(stopMgr)
		mgrStopped.Wait()
	}()

	// Create the GitLab object (the dummy)
	err = c.Create(context.TODO(), instance)
	if apierrors.IsInvalid(err) {
		t.Logf("failed to create object, got an invalid object error: %v", err)
		return
	}
	defer c.Delete(context.TODO(), instance)
	g.Expect(err).NotTo(gomega.HaveOccurred())

	// Ensure it DOES NOT reconcile the actual
	g.Consistently(requests, timeout).ShouldNot(gomega.Receive())

	// Ensure it DOES reconcile the dummy
	g.Eventually(subgrpRequests, timeout).Should(gomega.Receive(gomega.Equal(expectedRequest)))

}

func TestUpdateGitLabStatus(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	setup := fixtures.NewTestSetup(cfg, g)
	defer setup.Stop()

	reconciler := &ReconcileGitLab{
		Client: setup.GetClient(),
		scheme: setup.GetScheme(),
	}
	gitlab := &gitlabv1beta1.GitLab{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "foo-gitlab",
			Namespace: "default",
		},
		Spec: gitlabv1beta1.GitLabSpec{
			HelmRelease: "foo-release",
			Version:     "11.7",
			Revision:    "25",
			Templates: gitlabv1beta1.Templates{
				MigrationsTemplate: gitlabv1beta1.TemplateSpec{
					ConfigMapKey:  "foo-config-map-key",
					ConfigMapName: fixtures.MigrationConfigMap1.ObjectMeta.Name,
				},
			},
		},
	}

	g.Expect(
		setup.Create(gitlab),
	).ShouldNot(gomega.HaveOccurred())

	// Verify the object is created
	g.Expect(setup.Get(gitlab.ObjectMeta, gitlab)).NotTo(gomega.HaveOccurred())

	// Run test subject
	reconciler.setCondition(gitlab, *reconciler.newCondition(gitlabv1beta1.ConditionFailure, v1.ConditionTrue, "test", "message"))
	g.Expect(reconciler.updateGitLabStatus(gitlab)).ShouldNot(gomega.HaveOccurred())

	// Verify the status update
	g.Eventually(func() string {
		setup.Get(gitlab.ObjectMeta, gitlab)
		condition := reconciler.getCondition(*gitlab, gitlabv1beta1.ConditionFailure)
		if condition != nil {
			return condition.Reason
		}
		return ""
	}, timeout).Should(gomega.Equal("test"))

	// Test completed status
	g.Expect(reconciler.markGitLabDeployed(gitlab)).ShouldNot(gomega.HaveOccurred())

	g.Eventually(func() *gitlabv1beta1.Condition {
		setup.Get(gitlab.ObjectMeta, gitlab)
		return reconciler.getCondition(*gitlab, gitlabv1beta1.ConditionFailure)
	}, timeout).Should(gomega.BeNil())
	g.Expect(gitlab.Status.DeployedRevision).Should(gomega.Equal("25"))

	// Cleanup test resources
	g.Expect(
		setup.Delete(gitlab),
	).ShouldNot(gomega.HaveOccurred())
}

func TestRunMigrations(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	setup := fixtures.NewTestSetup(cfg, g)
	defer setup.Stop()

	reconciler := &ReconcileGitLab{
		Client: setup.GetClient(),
		scheme: setup.GetScheme(),
	}
	gitlab := &gitlabv1beta1.GitLab{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "foo-gitlab",
			Namespace: "default",
		},
		Spec: gitlabv1beta1.GitLabSpec{
			HelmRelease: "foo-release",
			Version:     "11.7",
			Templates: gitlabv1beta1.Templates{
				MigrationsTemplate: gitlabv1beta1.TemplateSpec{
					ConfigMapKey:  "foo-config-map-key",
					ConfigMapName: fixtures.MigrationConfigMap1.ObjectMeta.Name,
				},
			},
		},
	}

	// Load test resources
	configMap1 := fixtures.MigrationConfigMap1.DeepCopy()

	g.Expect(
		setup.Create(configMap1),
	).ShouldNot(gomega.HaveOccurred())

	// Wait for cache
	time.Sleep(1 * time.Second)

	// Run test subject
	errChan := make(chan error)
	go func() {
		_, err := reconciler.runMigrations(gitlab, "foo")
		errChan <- err
	}()

	// Give it some time to create the job
	time.Sleep(1 * time.Second)

	// Find migration job
	jobList := &batchv1.JobList{}
	g.Expect(
		setup.List(&client.ListOptions{}, jobList),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		jobList.Items,
	).To(gomega.HaveLen(1))
	migrationJob := &jobList.Items[0]

	// Update the status to simulate successful job termination
	migrationJob.Status.Succeeded = 1
	g.Expect(
		setup.UpdateStatus(migrationJob),
	).ShouldNot(gomega.HaveOccurred())

	// Wait for job to finish successfully
	err := <-errChan
	g.Expect(err).ShouldNot(gomega.HaveOccurred())

	// Verify the job
	g.Expect(
		setup.Get(migrationJob.ObjectMeta, migrationJob),
	).ShouldNot(gomega.HaveOccurred())

	g.Expect(
		migrationJob.ObjectMeta.Labels,
	).To(gomega.Equal(
		map[string]string{
			"migrationType":            "foo",
			"release":                  gitlab.Spec.HelmRelease,
			"gitlab.com/operator-name": "gitlab-operator",
		},
	))

	// Run cleanup
	reconciler.cleanUpMigrations(gitlab, migrationJob.ObjectMeta.Name)

	// Wait for cache
	time.Sleep(1 * time.Second)

	// Verify cleanup
	g.Expect(
		setup.Get(migrationJob.ObjectMeta, migrationJob),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		migrationJob.ObjectMeta.DeletionTimestamp,
	).ShouldNot(gomega.BeNil())
	g.Expect(
		migrationJob.ObjectMeta.Finalizers,
	).ShouldNot(gomega.BeEmpty())

	// Cleanup test resources
	g.Expect(
		setup.Delete(configMap1),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		setup.Delete(migrationJob),
	).ShouldNot(gomega.HaveOccurred())

}
func TestRunSharedSecrets(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	setup := fixtures.NewTestSetup(cfg, g)
	defer setup.Stop()

	reconciler := &ReconcileGitLab{
		Client: setup.GetClient(),
		scheme: setup.GetScheme(),
	}
	gitlab := &gitlabv1beta1.GitLab{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "foo-gitlab",
			Namespace: "default",
		},
		Spec: gitlabv1beta1.GitLabSpec{
			HelmRelease: "foo-release",
			Version:     "11.7",
			Templates: gitlabv1beta1.Templates{
				SharedSecretsTemplate: gitlabv1beta1.SharedSecretsSpec{
					ServiceAccountKey: "foo-service-account-key",
					RoleKey:           "foo-role-key",
					RoleBindingKey:    "foo-role-binding-key",
					TemplateSpec: gitlabv1beta1.TemplateSpec{
						ConfigMapName: fixtures.SharedSecretConfigMap1.ObjectMeta.Name,
						ConfigMapKey:  "foo-config-map-key",
					},
				},
			},
		},
	}

	// Load test resources
	configMap1 := fixtures.SharedSecretConfigMap1.DeepCopy()

	g.Expect(
		setup.Create(configMap1),
	).ShouldNot(gomega.HaveOccurred())

	// Wait for cache
	time.Sleep(1 * time.Second)

	// Run test subject
	errChan := make(chan error)
	go func() {
		_, _, err := reconciler.runSharedSecrets(gitlab)
		errChan <- err
	}()

	// Give it some time to create the job(s)
	time.Sleep(1 * time.Second)

	// Find shared secrets job(s)
	jobList := &batchv1.JobList{}
	g.Expect(
		setup.List(&client.ListOptions{}, jobList),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		jobList.Items,
	).To(gomega.HaveLen(1))
	sharedSecretJob := &jobList.Items[0]

	// Update the status to simulate successful job termination
	sharedSecretJob.Status.Succeeded = 1
	g.Expect(
		setup.UpdateStatus(sharedSecretJob),
	).ShouldNot(gomega.HaveOccurred())

	// Wait for job to finish successfully
	err := <-errChan
	g.Expect(err).ShouldNot(gomega.HaveOccurred())

	// Verify the job
	g.Expect(
		setup.Get(sharedSecretJob.ObjectMeta, sharedSecretJob),
	).ShouldNot(gomega.HaveOccurred())

	g.Expect(
		sharedSecretJob.ObjectMeta.Labels,
	).To(gomega.Equal(
		map[string]string{
			"release":                  gitlab.Spec.HelmRelease,
			"gitlab.com/operator-name": "gitlab-operator",
		},
	))

	// Run cleanup
	reconciler.cleanUpSharedSecrets(gitlab, sharedSecretJob.ObjectMeta.Name, "")

	// Wait for cache
	time.Sleep(1 * time.Second)

	// Verify cleanup
	g.Expect(
		setup.Get(sharedSecretJob.ObjectMeta, sharedSecretJob),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		sharedSecretJob.ObjectMeta.DeletionTimestamp,
	).ShouldNot(gomega.BeNil())
	g.Expect(
		sharedSecretJob.ObjectMeta.Finalizers,
	).ShouldNot(gomega.BeEmpty())

	// Cleanup test resources
	g.Expect(
		setup.Delete(configMap1),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		setup.Delete(sharedSecretJob),
	).ShouldNot(gomega.HaveOccurred())

}

func TestCleanupJob(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	setup := fixtures.NewTestSetup(cfg, g)
	defer setup.Stop()

	reconciler := &ReconcileGitLab{
		Client: setup.GetClient(),
		scheme: setup.GetScheme(),
	}
	gitlab := &gitlabv1beta1.GitLab{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "foo",
			Namespace: "default",
		},
	}

	// Load test resources
	job1 := fixtures.Job1.DeepCopy()
	pod1 := fixtures.Pod1.DeepCopy()
	pod2 := fixtures.Pod2.DeepCopy()
	pod3 := fixtures.Pod3.DeepCopy()
	pod1.ObjectMeta.Labels = map[string]string{
		"job-name": job1.ObjectMeta.Name,
	}
	pod2.ObjectMeta.Labels = map[string]string{
		"job-name": job1.ObjectMeta.Name,
	}

	g.Expect(
		setup.Create(job1),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		setup.Create(pod1),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		setup.Create(pod2),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		setup.Create(pod3),
	).ShouldNot(gomega.HaveOccurred())

	// Run test subject
	reconciler.cleanUpJob(gitlab, job1.ObjectMeta.Name)

	time.Sleep(1 * time.Second)

	// Verify test results
	g.Expect(
		setup.Get(job1.ObjectMeta, job1),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		job1.ObjectMeta.DeletionTimestamp,
	).ShouldNot(gomega.BeNil())
	g.Expect(
		job1.ObjectMeta.Finalizers,
	).ShouldNot(gomega.BeEmpty())
	g.Expect(
		setup.Get(pod1.ObjectMeta, pod1),
	).Should(gomega.MatchError(
		errors.NewNotFound(schema.GroupResource{
			Group:    "",
			Resource: "Pod",
		}, pod1.ObjectMeta.Name)),
	)
	g.Expect(
		setup.Get(pod2.ObjectMeta, pod2)).Should(gomega.MatchError(
		errors.NewNotFound(schema.GroupResource{
			Group:    "",
			Resource: "Pod",
		}, pod2.ObjectMeta.Name)))
	g.Expect(
		setup.Get(pod3.ObjectMeta, pod3),
	).ShouldNot(gomega.HaveOccurred())

	// Cleanup test resources
	g.Expect(
		setup.Delete(job1),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		setup.Delete(pod3),
	).ShouldNot(gomega.HaveOccurred())

}

func TestFindRunningMigrations(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	setup := fixtures.NewTestSetup(cfg, g)
	defer setup.Stop()

	reconciler := &ReconcileGitLab{
		Client: setup.GetClient(),
		scheme: setup.GetScheme(),
	}
	gitlab := &gitlabv1beta1.GitLab{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "foo-gitlab",
			Namespace: "default",
		},
		Spec: gitlabv1beta1.GitLabSpec{
			HelmRelease: "foo-release",
			Version:     "11.7",
			Templates: gitlabv1beta1.Templates{
				MigrationsTemplate: gitlabv1beta1.TemplateSpec{
					ConfigMapKey:  "foo-config-map-key",
					ConfigMapName: fixtures.MigrationConfigMap1.ObjectMeta.Name,
				},
			},
		},
	}

	// Load test resources
	job1 := fixtures.Job1.DeepCopy()
	job2 := fixtures.Job2.DeepCopy()
	job1.ObjectMeta.Labels = map[string]string{
		"release":       gitlab.Spec.HelmRelease,
		"migrationType": "pre",
	}
	job2.ObjectMeta.Labels = map[string]string{
		"release":       fmt.Sprintf("not-%s", gitlab.Spec.HelmRelease),
		"migrationType": "pre",
	}

	g.Expect(
		setup.Create(job1),
	).ShouldNot(gomega.HaveOccurred())
	g.Expect(
		setup.Create(job2),
	).ShouldNot(gomega.HaveOccurred())

	// Run test subject
	g.Expect(
		reconciler.findRunningMigrations(gitlab),
	).Should(gomega.BeTrue())

	// Remove the job
	g.Expect(
		setup.Delete(job1),
	).ShouldNot(gomega.HaveOccurred())

	// Run test subject
	g.Expect(
		reconciler.findRunningMigrations(gitlab),
	).Should(gomega.BeFalse())

	// Make sure the other job is still there
	g.Expect(
		setup.Get(job2.ObjectMeta, job2),
	).ShouldNot(gomega.HaveOccurred())

	// Cleanup remaining test resources
	g.Expect(
		setup.Delete(job2),
	).ShouldNot(gomega.HaveOccurred())

}

func TestPauseDeployments(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	setup := fixtures.NewTestSetup(cfg, g)
	defer setup.Stop()

	reconciler := &ReconcileGitLab{
		Client: setup.GetClient(),
		scheme: setup.GetScheme(),
	}

	instance := &gitlabv1beta1.GitLab{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "foo",
			Namespace: "default",
		},
		Spec: gitlabv1beta1.GitLabSpec{
			HelmRelease: "foo-release",
		},
	}

	deployment1 := fixtures.Deployment1.DeepCopy()

	g.Expect(setup.Create(deployment1)).ShouldNot(gomega.HaveOccurred())
	g.Expect(setup.Get(deployment1.ObjectMeta, deployment1)).ShouldNot(gomega.HaveOccurred())

	// It should be false by default
	g.Expect(deployment1.Spec.Paused).Should(gomega.BeFalse())

	// Pause the deployment
	err := reconciler.pauseDeployments(instance, "app=foo", true)
	g.Expect(err).ShouldNot(gomega.HaveOccurred())

	// Wait for cache
	time.Sleep(1 * time.Second)

	g.Expect(setup.Get(deployment1.ObjectMeta, deployment1)).ShouldNot(gomega.HaveOccurred())
	g.Expect(deployment1.Spec.Paused).Should(gomega.BeTrue())

	// Now test unpausing
	err = reconciler.pauseDeployments(instance, "app=foo", false)
	g.Expect(err).ShouldNot(gomega.HaveOccurred())

	// Wait for cache
	time.Sleep(1 * time.Second)

	g.Expect(setup.Get(deployment1.ObjectMeta, deployment1)).ShouldNot(gomega.HaveOccurred())
	g.Expect(deployment1.Spec.Paused).Should(gomega.BeFalse())

	// Clean up test Deployments
	g.Expect(setup.Delete(deployment1)).ShouldNot(gomega.HaveOccurred())
}

func TestPauseStatefulSets(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	setup := fixtures.NewTestSetup(cfg, g)
	defer setup.Stop()

	reconciler := &ReconcileGitLab{
		Client: setup.GetClient(),
		scheme: setup.GetScheme(),
	}

	instance := &gitlabv1beta1.GitLab{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "foo",
			Namespace: "default",
		},
		Spec: gitlabv1beta1.GitLabSpec{
			HelmRelease: "foo-helm-release",
		},
	}

	statefulset1 := fixtures.StatefulSet1.DeepCopy()

	// Create a test statefulset
	g.Expect(setup.Create(statefulset1)).ShouldNot(gomega.HaveOccurred())
	g.Expect(setup.Get(statefulset1.ObjectMeta, statefulset1)).ShouldNot(gomega.HaveOccurred())

	// StatefulSetSpec expects Replica and Partition to be pointers to an int32 object
	zero := int32Pointer(0)
	four := int32Pointer(4)

	// Verify that Partition is 0 by default
	g.Expect(statefulset1.Spec.UpdateStrategy.RollingUpdate.Partition).Should(gomega.Equal(zero))

	// Pause the StatefulSet
	err := reconciler.pauseStatefulSets(instance, "app=foo", true)
	g.Expect(err).ShouldNot(gomega.HaveOccurred())

	// Wait for cache
	time.Sleep(1 * time.Second)

	// Verify Partition gets set to 4 to match the Replica setting of the fixture
	g.Expect(setup.Get(statefulset1.ObjectMeta, statefulset1)).ShouldNot(gomega.HaveOccurred())
	g.Expect(statefulset1.Spec.UpdateStrategy.RollingUpdate.Partition).Should(gomega.Equal(four))

	// Unpause the StatefulSet
	err = reconciler.pauseStatefulSets(instance, "app=foo", false)
	g.Expect(err).ShouldNot(gomega.HaveOccurred())

	// Wait for cache
	time.Sleep(1 * time.Second)

	// Verify Partition gets set back to 0
	g.Expect(setup.Get(statefulset1.ObjectMeta, statefulset1)).ShouldNot(gomega.HaveOccurred())
	g.Expect(statefulset1.Spec.UpdateStrategy.RollingUpdate.Partition).Should(gomega.Equal(zero))

	// Clean up test StatefulSet
	g.Expect(setup.Delete(statefulset1)).ShouldNot(gomega.HaveOccurred())
}

func TestPauseJobs(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	setup := fixtures.NewTestSetup(cfg, g)
	defer setup.Stop()

	reconciler := &ReconcileGitLab{
		Client: setup.GetClient(),
		scheme: setup.GetScheme(),
	}

	instance := &gitlabv1beta1.GitLab{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "foo",
			Namespace: "default",
		},
		Spec: gitlabv1beta1.GitLabSpec{
			HelmRelease: "foo-helm-release",
		},
	}

	job1 := fixtures.Job1.DeepCopy()

	// Create a test Job
	g.Expect(setup.Create(job1)).ShouldNot(gomega.HaveOccurred())
	g.Expect(setup.Get(job1.ObjectMeta, job1)).ShouldNot(gomega.HaveOccurred())

	defaultParallelism := int32Pointer(1)
	zero := int32Pointer(0)

	// Verify that Parallelism is 0 by default
	g.Expect(job1.Spec.Parallelism).Should(gomega.Equal(defaultParallelism))

	// Pause the Job
	err := reconciler.pauseJobs(instance, "app=foo", true)
	g.Expect(err).ShouldNot(gomega.HaveOccurred())

	// Wait for cache
	time.Sleep(1 * time.Second)

	g.Expect(setup.Get(job1.ObjectMeta, job1)).ShouldNot(gomega.HaveOccurred())
	g.Expect(job1.Spec.Parallelism).Should(gomega.Equal(zero))

	// Unpause the Job
	err = reconciler.pauseJobs(instance, "app=foo", false)
	g.Expect(err).ShouldNot(gomega.HaveOccurred())

	// Wait for cache
	time.Sleep(1 * time.Second)

	g.Expect(setup.Get(job1.ObjectMeta, job1)).ShouldNot(gomega.HaveOccurred())
	g.Expect(job1.Spec.Parallelism).Should(gomega.Equal(defaultParallelism))

	// Clean up test Job
	g.Expect(setup.Delete(job1)).ShouldNot(gomega.HaveOccurred())
}

func TestCheckStatefulSetFinished(t *testing.T) {
	g := gomega.NewGomegaWithT(t)

	setup := fixtures.NewTestSetup(cfg, g)
	defer setup.Stop()

	reconciler := &ReconcileGitLab{
		Client: setup.GetClient(),
		scheme: setup.GetScheme(),
	}

	instance := &gitlabv1beta1.GitLab{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "foo",
			Namespace: "default",
		},
		Spec: gitlabv1beta1.GitLabSpec{
			HelmRelease: "foo-helm-release",
		},
	}

	statefulset1 := fixtures.StatefulSet1.DeepCopy()

	g.Expect(setup.Create(statefulset1)).ShouldNot(gomega.HaveOccurred())
	g.Expect(setup.Get(statefulset1.ObjectMeta, statefulset1)).ShouldNot(gomega.HaveOccurred())

	g.Eventually(func() bool {
		for {
			deployed, err := reconciler.checkStatefulSetFinished(instance, "app=foo")
			if err != nil {
				return false
			}
			if deployed != true {
				time.Sleep(5 * time.Second)
				continue
			}
			return deployed
		}
	}, gitalyRollingUpdateTimeout).Should(gomega.BeTrue())
}
